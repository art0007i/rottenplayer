package io.tr3y.rottenplayer;

import org.bukkit.DyeColor;

public class FullColorPolicy implements ColorPolicy {

	@Override
	public DyeColor getColor(char c) {
		switch (c) {
		case '0':
			return DyeColor.WHITE;
		case '1':
			return DyeColor.ORANGE;
		case '2':
			return DyeColor.MAGENTA;
		case '3':
			return DyeColor.LIGHT_BLUE;
		case '4':
			return DyeColor.YELLOW;
		case '5':
			return DyeColor.LIME;
		case '6':
			return DyeColor.PINK;
		case '7':
			return DyeColor.GRAY;
		case '8':
			return DyeColor.LIGHT_GRAY;
		case '9':
			return DyeColor.CYAN;
		case 'A':
			return DyeColor.PURPLE;
		case 'B':
			return DyeColor.BLUE;
		case 'C':
			return DyeColor.BROWN;
		case 'D':
			return DyeColor.GREEN;
		case 'E':
			return DyeColor.RED;
		case 'F':
			return DyeColor.BLACK;

		// Anything else we don't know what to do with, just make black.
		default:
			return DyeColor.BLACK;
		}
	}

}
