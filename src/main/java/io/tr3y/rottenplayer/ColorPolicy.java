package io.tr3y.rottenplayer;

import org.bukkit.DyeColor;

public interface ColorPolicy {
	DyeColor getColor(char c);
}
