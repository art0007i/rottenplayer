#!/usr/bin/env python3

import sys

from PIL import Image
from math import sqrt

COLORS = (
	(249, 255, 254),
	(249, 128,  29),
	(199,  78, 189),
	( 58, 179, 218),
	(254, 216,  61),
	(128, 199,  31),
	(243, 139, 170),
	( 71,  79,  82),
	(157, 157, 151),
	( 22, 156, 156),
	(137,  50, 184),
	( 60,  68, 170),
	(131,  84,  50),
	( 94, 124,  22),
	(176,  46,  38),
	( 29,  29,  33)
)

def closest_color(rgb):
	r, g, b = rgb
	color_diffs = []
	for i in range(len(COLORS)):
		cr, cg, cb = COLORS[i]
		color_diff = sqrt(abs(r - cr)**2 + abs(g - cg)**2 + abs(b - cb)**2)
		color_diffs.append((color_diff, i))
	return format(min(color_diffs)[1], 'X')


def process_frame(img, w_samples, h_samples):
	samples = []

	# Do some math to figure out how to choose our samples.
	(w, h) = img.size
	w_inc = w // w_samples
	w_free = w - (w_inc * w_samples)
	h_inc = h // h_samples
	h_free = h - (h_inc * h_samples)

	# Now actually get them.
	for y_samp in range(h_samples):
		y_pos = h_inc * y_samp + (h_free / 2)
		for x_samp in range(w_samples):
			x_pos = w_inc * x_samp + (w_free / 2)
			rgb = img.getpixel((x_pos, y_pos))

			samples.append(closest_color(rgb))

	return samples

outfile = sys.argv[1]
dimx = int(sys.argv[2])
dimy = int(sys.argv[3])

frames = []

with open(outfile, 'w') as f:
	f.write("%s %s\n" % (dimx, dimy))

	cur = 0
	while True:
		img_path = "image_%s.png" % (cur + 1)
		try:
			if cur > 1 and cur % 100 == 0:
				print('Processed', cur, 'frames')

			img = Image.open(img_path)
			samps = process_frame(img, dimx, dimy)
			samps.append('\n')

			buf = ''.join([str(s) for s in samps])
			f.write(buf)

			cur += 1
		except:
			print('Found', cur - 1, 'images')
			break

print('Done!')
